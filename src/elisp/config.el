(setq package-archives
      '(("melpa" . "https://melpa.org/packages/")
        ("gnu" . "https://elpa.gnu.org/packages/")
        ("org" . "http://orgmode.org/elpa/")))

(require 'package)

(package-initialize)

(package-refresh-contents)

(package-install 'org-plus-contrib)

(require 'org)

(defun my/set-org-project-vars (org-path posts-path static-path)

  (setq org-publish-project-alist
        `(("blog-org"
           :base-directory ,org-path
           :base-extension "org"
           :publishing-directory ,posts-path
           :recursive t
           :publishing-function org-html-publish-to-html
           :headline-levels 4
           :html-extension "html"
           :body-only t
           :with-toc nil
           :section-numbers nil
           :table-of-contents nil
           :author "Troy de Freitas"
           :email "me@ntdef.com")
          ("blog-static"
           :base-directory ,org-path
           :base-extension "css\\|js\\|png\\|jpg\\|ico\\|gif\\|pdf\\|mp3\\|flac\\|ogg\\|swf\\|php\\|markdown\\|md\\|html\\|htm\\|sh\\|xml\\|gz\\|bz2\\|vcf\\|zip\\|txt\\|tex\\|otf\\|ttf\\|eot\\|rb\\|yml\\|htaccess\\|gitignore\\|svg"
           :publishing-directory ,static-path
           :recursive t
           :publishing-function org-publish-attachment)
          ("blog" :components ("blog-org" "blog-static")))))

(my/set-org-project-vars "org" "_posts" "assets")
